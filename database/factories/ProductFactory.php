<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
		$rand = rand(1, 10) * 1000;
	
    return [
				'name' => 'Produk '. rand(1, 100),
				'description' => 'Description '. rand(1, 100),
				'stock' => rand(1, 100),
				'price' => $rand,
				'category_id' => rand(1, 10)
    ];
});
