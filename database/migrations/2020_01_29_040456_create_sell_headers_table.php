<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_headers', function (Blueprint $table) {
						$table->bigIncrements('id');
						$table->string('invoice');
						$table->unsignedBigInteger('user_id');
						$table->foreign('user_id')->references('id')->on('users');
						$table->double('discount');
						$table->double('cash');
						$table->double('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_headers');
    }
}
