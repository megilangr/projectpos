<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_details', function (Blueprint $table) {
						$table->bigIncrements('id');
						$table->unsignedBigInteger('sell_header_id');
						$table->foreign('sell_header_id')->references('id')->on('sell_headers');
						$table->unsignedBigInteger('product_id')->references('id')->on('products');
						$table->double('qty');
						$table->double('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_details');
    }
}
