<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::firstOrCreate([
            'name' => 'Product A',
            'description' => 'A A',
            'stock' => 10,
            'price' => 10000,
            'category_id' => 1,
        ]);
        Product::firstOrCreate([
            'name' => 'Product B',
            'description' => 'B B',
            'stock' => 20,
            'price' => 20000,
            'category_id' => 2,
        ]);
        Product::firstOrCreate([
            'name' => 'Product AB',
            'description' => 'AB AB',
            'stock' => 15,
            'price' => 15000,
            'category_id' => 1,
        ]);
        Product::firstOrCreate([
            'name' => 'Product BA',
            'description' => 'BA BA',
            'stock' => 25,
            'price' => 25000,
            'category_id' => 2,
        ]);
    }
}
