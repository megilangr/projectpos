<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::firstOrCreate([ 'name' => 'Kategori A' ]);
        Category::firstOrCreate([ 'name' => 'Kategori B' ]);
    }
}
