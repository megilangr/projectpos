<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use Cart;

class CartController extends Controller
{
    public function add(Request $request)
		{
			try {
				$product = Product::findOrFail($request->id);

				Cart::session(auth()->user()->id)->add([
					'id' => $request->id,
					'name' => $request->name,
					'price' => $request->price,
					'quantity' => $request->qty,
					'associatedModel' => $product
				]);

				$data = [
					'message' => 'Berhasil Memasukan Kedalam Keranjang'
				];

				return response()->json($data, 200);
			} catch (\Exception $e) {
				$data = [
					'code' => '404',
					'message' => 'Product Salah !',
					'error' => $e
				];
				return response()->json($data, 500);
			}
		}

		public function getCart(Request $request)
		{
			$cart = Cart::session($request->userId)->getContent();
			$data = [
				'cart' => $cart,
				'totalItem' => $cart->count(),
				'totalQty' => Cart::session(auth()->user()->id)->getTotalQuantity(),
				'totalPay' => Cart::session(auth()->user()->id)->getTotal()
			];
			return response()->json($data, 200);
		}

		public function updateQty(Request $request)
		{
			$cart = Cart::session(auth()->user()->id)->update($request->id, [
				'quantity' => [
					'relative' => false,
					'value' => $request->qty
				]
			]);
			
			$data = [
				'message' => 'Berhasil Mengubah Qty Produk !'
			];
			return response()->json($data, 200);
		}

		public function deleteItem(Request $request)
		{
			$cart = Cart::session(auth()->user()->id)->remove($request->id);
			$data = [
				'message' => 'Berhasil Menghapus Item !'
			];
			return response()->json($data, 200);
		}
}
