<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\SellDetail;
use App\SellHeader;
use Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
				if ($request->ajax()) {
					$product = Product::with('category')->where('name', 'like', '%'.$request->search.'%')->get();
					if (empty($product)) {
						$data = [
							'message' => 'Tidak Di-Temukan Data Produk !',
						];
						return response()->json($data, 200);
					} else {
						return response()->json($product, 200);
					}
				}

				$product = Product::with('category')->get();
        return view('backend.pos.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
				// 
		}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
			$user = Auth::user();
			if (Cart::session($user->id)->isEmpty()) {
				$data = [
					'code' => '400', 
					'message' => 'Silahkan Pilih Produk untuk Transaksi',
				];
				return response()->json($data, 200);
			} else {
				$cart = Cart::session($user->id)->getContent();	

				DB::beginTransaction();
				try {
					$header = DB::table('sell_headers')->insert([
						'invoice' => 'SL'.time(),
						'user_id' => $user->id,
						'discount' => $request->discount,
						'cash' => $request->cash,
						'total' => Cart::session($user->id)->getTotal(),
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
					]);

					$header_id = DB::getPdo()->lastInsertId();

					foreach ($cart as $item) {
						$detail = DB::table('sell_details')->insert([
							'sell_header_id' => $header_id,
							'product_id' => $item->id,
							'qty' => $item->quantity,
							'price' => $item->price,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s'),
						]);
					}

					DB::commit();

					$clearCart = Cart::session($user->id)->clear();

					$data = [
						'code' => '200',
						'message' => 'Transaksi Selesai !'
					];

					return response()->json($data, 200);

				} catch (\Exception $e) {
					DB::rollback();
					$data = [
						'code' => '400', 
						'message' => 'Terjadi Kesalahan Saat Menyiman Kedalam Data',
						'error' => $e
					];

					return response()->json($data, 200);
				}
			}

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		}
		
		public function list(Request $request)
		{
			$sell = SellHeader::orderBy('id', 'DESC')->get();

			return view('backend.pos.list', compact('sell'));
		}
}
