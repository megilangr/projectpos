<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Support\Facades\Crypt;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
				if($request->ajax()) {
					$data = Category::get();
					return DataTables::of($data)
						->addIndexColumn()
						->addColumn('action', function($data) {
							return '
							<form action="'.route('category.destroy', Crypt::encrypt($data->id)).'" method="post">
								<a href="'.route('category.edit', Crypt::encrypt($data->id)).'" class="btn btn-warning btn-sm">
									<i class="fa fa-edit"></i>
								</a>

								<input type="hidden" name="_token" value="'.csrf_token().'">
								<input type="hidden" name="_method" value="DELETE" >
								<button class="btn btn-danger btn-sm">
									<i class="fa fa-trash"></i>
								</button>	
							</form>
							';
						})
						->rawColumns(['action'])
						->make(true);
				}
			
        return view('backend.category.index');
    }
		
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
					'name' => 'required|string|unique:categories,name'
				]);

				try {
					$category = Category::firstOrCreate([
						'name' => $request->name
					]);

					session()->flash('success', 'Data Kategori di-Tambahkan !');
					return redirect(route('category.index'));
				} catch (\Exception $e) {
					dd($e);
					session()->flash('error', 'Mohon Maaf Terjadi Kesalahan !');
					return redirect()->back();
				}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
				$id = $this->cry($id);
				try {
					$edit = Category::findOrFail($id);

					return view('backend.category.index', compact('edit'));
				} catch (\Exception $e) {
					return redirect()->back();
				}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
				$this->validate($request, [
					'name' => 'required|string|unique:categories,name'
				]);

				$id = $this->cry($id);
				try {
					$category = Category::findOrFail($id);
					$category->update([
						'name' => $request->name
					]);
					session()->flash('success', 'Berhasil Mengubah Data Kategori !');
					return redirect(route('category.index'));
				} catch (\Exception $e) {
					return redirect()->back();
				}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
				$id = $this->cry($id);
				try {
					$category = Category::findOrFail($id);
					$category->delete();
					session()->flash('success', 'Data Kategori di-Hapus');
					return redirect(route('category.index'));
				} catch (\Exception $e) {
					dd($e);
				}
		}
		
		public function cry($id)
		{
			try {
				$decrypted = Crypt::decrypt($id);
				return $decrypted;
			} catch (Illuminate\Contracts\Encryption\DecryptException $e) {
				dd($e);
			}
		}

}
