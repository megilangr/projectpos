<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use DataTables;
use Illuminate\Support\Facades\Crypt;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
				if ($request->ajax()) {
					$data = Product::with('category')->get();
					return DataTables::of($data)
						->addIndexColumn()
						->addColumn('action', function($data) {
							return '
							<form action="'.route('category.destroy', Crypt::encrypt($data->id)).'" method="post">
								<a href="'.route('category.edit', Crypt::encrypt($data->id)).'" class="btn btn-warning btn-sm">
									<i class="fa fa-edit"></i>
								</a>

								<input type="hidden" name="_token" value="'.csrf_token().'">
								<input type="hidden" name="_method" value="DELETE" >
								<button class="btn btn-danger btn-sm">
									<i class="fa fa-trash"></i>
								</button>	
							</form>
							';
						})
						->addColumn('price', function($data) {
							return "Rp. ".number_format($data->price, 0, ',', '.');
						})
						->rawColumns(['action'])
						->make(true);
				}

        return view('backend.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
				if ($request->ajax()) {
					$a = $request->ajax();
					return response()->json($a, 200);
				}

				$category = Category::orderBy('name')->get();

        return view('backend.product.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
