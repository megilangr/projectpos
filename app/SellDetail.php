<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellDetail extends Model
{
		protected $fillable = [
			'sell_header_id', 'product_id', 'qty', 'price' 
		];

		public function product()
		{
			return $this->belongsTo('App\Products')->withDefault('N/A');
		}

		public function header()
		{
			return $this->hasOne('App\SellHeader', 'sell_id');
		}
}
