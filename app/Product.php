<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'name',
		'description',
		'stock',
		'price',
		'category_id'
	];

	public function category()
	{
		return $this->belongsTo('App\Category')->withDefault('N/A');
	}

	public function sell()
	{
		return $this->belongsTo('App\SellDetail')->withDefault('N/A');
	}
}
