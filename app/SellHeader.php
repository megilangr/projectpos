<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellHeader extends Model
{
    protected $fillable = [
			'invoice', 'user_Id', 'total'
		];

		public function user()
		{
			return $this->hasOne('App\User');
		}

		public function detail()
		{
			return $this->hasMany('App\SellDetail');
		}
}
