<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\MainController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'Admin'], function () {
    Route::get('/', 'Backend\MainController@index')->name('admin.main');

    Route::group(['middleware' => ['role:Admin']], function () {
        Route::resource('role', 'Backend\RoleController');
        Route::resource('permission', 'Backend\PermissionController');
        Route::resource('user', 'Backend\UserController');
			});
		});
		
Route::resource('category', 'Backend\CategoryController');
Route::resource('product', 'Backend\ProductController');	
Route::resource('pos', 'Backend\PosController');
Route::get('pos-list', 'Backend\PosController@list')->name('pos.list');

// Route::get('pos/cart', 'Backend\CartController@getCart')->name('cart.getCart');
Route::post('pos/cart', 'Backend\CartController@getCart')->name('cart.getCart');
Route::post('pos/addToCart', 'Backend\CartController@add')->name('cart.addToCart');
Route::post('pos/updateQtyCart', 'Backend\CartController@updateQty')->name('cart.updateQtyCart');
Route::post('pos/deleteItem', 'Backend\CartController@deleteItem')->name('cart.deleteItem');