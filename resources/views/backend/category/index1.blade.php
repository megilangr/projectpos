@extends('backend.layout.master')

@section('judul')
		Management Kategori Produk
@endsection

@section('content')
<div class="col-5 col-md-5">
	<div class="card card-dark">
		<div class="card-header">
			<h4 class="card-title">
				Form Kategori
			</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-12">
					@if (isset($edit))
						<form action="{{ route('category.update', Crypt::encrypt($edit->id)) }}" method="post">
							@method('PUT')
					@else
						<form action="{{ route('category.store') }}" method="post">
					@endif
						@csrf
						<div class="form-group">
							<label for="">Nama Kategori :</label>
							<input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" value="{{ isset($edit) ? $edit->name:old('name') }}" required>
							<p class="text-danger text-sm">
								{{ $errors->first('name') }}
							</p>
						</div>
						<div class="form-group">
							<div class="row">
								@if (isset($edit))
									<div class="col-4">
										<button type="submit" class="btn btn-success btn-sm btn-block btn-flat">
											<i class="fa fa-check"></i> &ensp;
											Simpan
										</button>
									</div>
									<div class="col-4">
										<button type="reset" class="btn btn-warning btn-sm btn-block btn-flat">
											<i class="fa fa-undo"></i>
											Reset
										</button>
									</div>
									<div class="col-4">
										<a href="{{ route('category.index') }}" class="btn btn-danger btn-sm btn-block btn-flat">
											<i class="fa fa-times"></i>
											Cancel
										</a>
									</div>

								@else
									<div class="col-6">
										<button type="submit" class="btn btn-success btn-sm btn-block btn-flat">
											<i class="fa fa-check"></i> &ensp;
											Tambah Data Kategori
										</button>
									</div>
									<div class="col-6">
										<button type="reset" class="btn btn-danger btn-sm btn-block btn-flat">
											<i class="fa fa-undo"></i>
											Reset Input
										</button>
									</div>
								@endif
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-12 col-md-12">
	<div class="card card-dark">
		<div class="card-header">
			<h4 class="card-title">
				Data Kategori
			</h4>
		</div>
		<div class="card-body">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-hover data-table">
						<thead>
							<tr>
								<th width="50px">#</th>
								<th>Nama Kategori</th>
								<th width="100px">Aksi</th>
							</tr>
						</thead>
						<tbody>
	
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
		
<script>
	$(document).ready(function() {
	
		var table = $('.data-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ route('category.index') }}",
			columns: [
				{
					data: 'DT_RowIndex',
					name: 'DT_RowIndex'
				},
				{
					data: 'name',
					name: 'name',
				},
				{
					data: 'action',
					name: 'action',
					orderable: false,
					searchable: false
				}
			]
		});
	
	});
</script>
@endsection