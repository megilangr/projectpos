@extends('backend.app')

@section('content')
<div class="col-md-12" style="margin-top: 20px;">
	<div class="col-md-6" style="padding: 0;">
		<div class="portlet light bordered" style="border-radius: 0px;">
			<div class="portlet-title">
				<div class="col-md-12" style="padding: 0px;">
					<h4>Management Kategori</h4>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-hover data-table">
						<thead>
							<tr>
								<th width="50px">#</th>
								<th>Nama Kategori</th>
								<th width="100px">Aksi</th>
							</tr>
						</thead>
						<tbody>
	
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<!-- jQuery -->
<script src="{{ asset('') }}plugins/jquery/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		var table = $('.data-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ route('category.index') }}",
			columns: [
				{
					data: 'DT_RowIndex',
					name: 'DT_RowIndex'
				},
				{
					data: 'name',
					name: 'name',
				},
				{
					data: 'action',
					name: 'action',
					orderable: false,
					searchable: false
				}
			]
		});
	
	});
</script>