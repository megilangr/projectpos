@extends('backend.app')

@section('content')
	<div class="col-md-12" style="margin-top: 20px;">
		<div class="portlet light bordered" style="border-radius: 0px;">
			<div class="portlet-title">
				<h4>Data Transaksi</h4>
			</div>
			<div class="portlet-body">
				<div class="table-responsive table-scrollable">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>Nomor Transaksi</th>
								<th>Jumlah Produk</th>
								<th>Total</th>
								<th>Pembayaran</th>
								<th>Kembalian</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							@forelse ($sell as $item)
								<tr>
									<td><h5>{{ $loop->iteration }}</h5></td>
									<td><h5>#{{ $item->invoice }}</h5></td>
									<td><h5>{{ count($item->detail) }} Produk</h5></td>
									<td><h5>Rp. {{ number_format($item->total, 0, ',', '.') }}</h5></td>
									<td><h5>Rp. {{ number_format($item->cash, 0, ',', '.') }}</h5></td>
									<td><h5>Rp. {{ number_format($item->cash - $item->total, 0, ',', '.') }}</h5></td>
									<td>
										<a href="javascript:void(0)" class="btn btn-warning btn-sm">
											<i class="fa fa-edit"></i>
										</a>
									</td>
								</tr>
							@empty
								<tr>
									<td colspan="7">
										<h5>Belum Ada Penjualan</h5>
									</td>
								</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection