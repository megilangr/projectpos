@extends('backend.app')

@section('content')
<script>
	var mustPay = "{{ Cart::session(auth()->user()->id)->getTotal() }}";
	var chrgs = 0;
	var disc = 0;
	var cash = 0;

	function editQty(row, name, qty, harga) {
		$('#qtyItem'+row).empty();
		$('#qtyItem'+row).append(`
			<input type="number" name="qty" onkeyup="editQtyPrice(` + row + `, `+ harga +`)" id="qty${row}" class="form-control updateQtyProduct" style="border-radius: 0px;" min="1" value="${qty}" data-id="${row}" data-name="${name}" data-price="${harga}">
		`);
		$('#qty'+row).focus();
	}

	function editQtyPrice(row, harga) {
		var qty = $('#qty' + row).val();
		var sub = qty * harga;
		$('#priceItem' + row).text("Rp. " + sub);
	}

	function deleteFromCart(row) {
		$.ajax({
			url: "{{ route('cart.deleteItem') }}",
			method: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				id: row
			},
			success: function(data) {
				getCart();
			}
		});
	}
</script>

<div class="col-md-12" style="margin-top: 20px;">
    <div class="col-md-6">
        <div class="portlet light bordered" style="border-radius: 0px;">
            <div class="portlet-title">
                <div class="col-md-12" style="padding: 0px;">
                    <h4>Point of Sales - Transaksi Penjualan</h4>
                    <hr>
                    <input type="text" name="search" id="search" class="form-control" autocomplete="off" placeholder="Cari Produk ....">
                </div>
            </div>
            <div class="portlet-body">
								<div class="searched" style="height: 400px; max-height: 450px; overflow: auto;" hidden>
									<div class="alert alert-danger" id="notFound" hidden>
										<p>Produk tidak di-Temukan !</p>
									</div>

									<div class="panel panel-default" id="searchPanel" hidden>
											<div class="panel-heading">
												Hasil Pencarian Produk :
											</div>
											<div class="panel-body" id="searchPanelBody">
													
											</div>
									</div>
								</div>
								<div class="category" style="height: 400px; max-height: 450px; overflow: auto;">
									@php $noImg = 1 @endphp
									@forelse ($product as $item)

										<div class="col-md-4 col-xs-4 col-sm-4" style="margin-top: 5px; margin-bottom: 10px; padding-left: 5px;">
											<div class="product" style="padding: 2px; border: 1px solid #b3aaaa;" data-id="{{ $item->id }}" data-name="{{ $item->name }}" data-price="{{ $item->price }}">
												{{-- <img src="{{ asset('baseImage/no-image.png') }}" alt="#" class="img-responsive"> --}}
												<img src="https://res.cloudinary.com/bacapaja/image/upload/v1580289120/example-{{ $noImg }}.png" alt="#" class="img-responsive">
												<hr style="margin: 5px 0px; border-top: 1px solid #b3aaaa;">
												<p class="text-center" style="margin: 2px;">
													{{ $item->name }}
												</p>
											</div>
										</div>
										@if ($noImg == 18)
											@php $noImg = 1 @endphp
										@endif
										@php $noImg++ @endphp
									@empty
										<p class="text-center" style="margin: 10px 0px; padding: 5px; border: 1px solid #000000;">
											Belum Ada Produk !
										</p>
									@endforelse
									{{-- @forelse ($category as $item)
										<div class="panel panel-default">
                        <div class="panel-heading">
													{{ $item->name }}
                        </div>
                        <div class="panel-body">
													@forelse ($item->product as $item2)
														<div class="col-md-4 col-xs-4 col-sm-4" style="margin-top: 10px; margin-bottom: 20px;">
															<div class="product" style="padding: 2px; border: 1px solid #b3aaaa;" data-id="{{ $item2->id }}" data-name="{{ $item2->name }}" data-price="{{ $item2->price }}">
																<img src="{{ asset('baseImage/no-image.png') }}" alt="#" class="img-responsive">
																<hr style="margin: 5px 0px; border-top: 1px solid #b3aaaa;">
																<p class="text-center" style="margin: 2px;">
																	{{ $item2->name }}
																</p>
															</div>
														</div>
													@empty
														<div class="col-md-4 col-xs-4 col-sm-4" style="margin-top: 10px; margin-bottom: 20px;">
															<div>
																<p class="text-center" style="margin: 2px;">
																	Belum Ada Produk
																</p>
															</div>
														</div>
													@endforelse
                        </div>
										</div>
									@empty
										<p class="text-center" style="margin: 10px;">
											Belum Ada Kategori & Produk
										</p>
									@endforelse --}}
                </div>
            </div>
        </div>
		</div>
		<div class="col-md-6">
        <div class="portlet light bordered" style="border-radius: 0px;">
					<div class="portlet-title">
						<div class="col-md-12" style="padding: 0px;">
							<h4><i class="fa fa-shopping-cart"></i> &ensp; Keranjang Pembelian</h4>
						</div>
					</div>
					<div class="porlet-body">
						<div class="cart" style="min-height: 200px; max-height: 250px; overflow: auto;">
							<div class="table-responsive">
								<table class="table table-bordered" style="margin-bottom: 0px;" id="cart">
									<thead>
										<tr>
											<th width="35%">Produk</th>
											<th width="25%">Qty</th>
											<th width="30%">Sub Total</th>
											<th width="10%">Action</th>
										</tr>
									</thead>
									<tbody id="cartBody"> 
										@forelse (Cart::session(Auth()->user()->id)->getContent() as $item)
											<tr>
												<td>
													<h5>{{ $item->name }}</h5>
												</td>
												<td id="qtyItem{{ $item->id }}">
													<h5>{{ $item->quantity }} &ensp; <a href="javascript:void(0)" class="glyphicon glyphicon-pencil" onclick="editQty({{ $item->id }}, '{{ $item->name }}', {{ $item->quantity }}, {{ $item->price }})"></a></h5>
												</td>
												<td>
													<h5 id="priceItem{{ $item->id }}">Rp. {{ $item->quantity * $item->price }}</h5>			
												</td>
												<td class="text-center">
													<a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="deleteFromCart({{$item->id}})">
														<i class="fa fa-trash"></i>
													</a>
												</td>
											</tr>
										@empty
											<tr id="emptyCart">
												<td colspan="4">
													Belum Ada Produk dalam Keranjang
												</td>
											</tr>
										@endforelse
									</tbody>
								</table>
							</div>
						</div>
						<div class="action">
							<form action="{{ route('pos.store') }}" method="POST">
							@csrf
							<div class="panel panel-default" style="margin-bottom: 0px; border-radius: 0px;">
								<div class="panel-heading" style="padding: 0px;">
									<div class="table-responsive">
										<table class="table" style="margin-bottom: 0px;">
											<thead>
												<tr>
													<th width="35%">Total Produk : </th>
													<th width="25%">Jumlah Item : </th>
													<th width="30%">Jumlah Kotor : </th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td id="totalProduct">{{ Cart::session(auth()->user()->id)->getContent()->count() }} Produk</td>
													<td id="totalQty">{{ Cart::session(auth()->user()->id)->getTotalQuantity() }} Item</td>
													<td id="totalSub">Rp. {{ Cart::session(auth()->user()->id)->getTotal() }}</td>
													<td></td>
												</tr>
												<tr>
													<th colspan="2" class="text-right">
														<h5>
															<b>
																Diskon :
															</b>
														</h5>
													</th>
													<td>
														<input type="number" name="discount" id="discount" class="form-control" onkeyup="countDiscount()" style="border-radius: 0px;" min="0" value="0" required>
													</td>
													<td class="text-center">
														<h5>
															<b>
																<i class="fa fa-minus"></i></td>
															</b>
														</h5>
												</tr>
												<tr>
													<th colspan="2" class="text-right">Total : </th>
													<td id="totalAkhir">Rp. {{ Cart::session(auth()->user()->id)->getTotal() }}</td>
													<td></td>
												</tr>
												<tr>
													<th colspan="2" class="text-right">
														<h5>
															<b>
																	Nominal Uang Tunai : 
															</b>
														</h5>
													</th>
													<td colspan="2">
														<input type="number" name="cash" id="cash" class="form-control" onkeyup="countCharges()" style="border-radius: 0px;" min="0" value="0" required>
													</td>
												</tr>
												<tr>
													<th colspan="2" class="text-right">Kembalian : </th>
													<td id="charge">0</td>
												</tr>
												<tr>
													<td colspan="4">
														<button type="button" class="btn btn-success btn-block" id="end" disabled>
															<i class="fa fa-check"></i> &ensp; Selesai Transaksi
														</button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							</form>
						</div>
					</div>
        </div>			
		</div>
</div>
@endsection

@section('javascript')
<script>
	no = 1;
</script>

<script>
	function deleteRow(row) {
		$('#item' + row).remove();
	}

	function cari(nama) {
		$.ajax({
			url: "{{ route('pos.index') }}",
			method: 'GET',
			data: {
				search: nama
			},
			success: function(data) {
				if (data.length == 0) {
					$('#notFound').show();
					$('#searchPanel').hide();
				} else {
					$('#notFound').hide();
					$('#searchPanel').show();
					$('.searchedItem').remove();

					$('#searched').append(nama);
					data.forEach(data => {
						$('#searchPanelBody').append(`
							<div class="col-md-4 col-xs-4 col-sm-4 searchedItem" style="margin-top: 10px; margin-bottom: 20px;">
								<div class="product" style="padding: 2px; border: 1px solid #b3aaaa;" data-id="`+ data.id +`" data-name="`+ data.name +`" data-price="`+ data.price +`">
									<img src="https://res.cloudinary.com/bacapaja/image/upload/v1580289120/example-3.png" alt="#" class="img-responsive">
									<hr style="margin: 5px 0px; border-top: 1px solid #b3aaaa;">
									<p class="text-center" style="margin: 2px;">
										`+ data.name +`
									</p>
								</div>
							</div>
						`);
					});
				}
			}
		});
	}

	function qtyHarga(row, price) {
		var qty = $('#qty' + row).val();
		var sub = qty * price;
		$('#price' + row).text("Rp. " + sub);
		$('#price' + row).data('sub', sub);
	}

	function addToCart(id, name, price, qty) {
		$.ajax({
			url: "{{ route('cart.addToCart') }}",
			method: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				id: id,
				name: name,
				price: price,
				qty: qty
			},
			success: function(data) {
				// console.log(data);
				getCart();
			}, 
			error: function(data) {
				// console.log(data);
			}
		});
	}

	function updateQtyCart(id, name, price, qty) {
		$.ajax({
			url: "{{ route('cart.updateQtyCart') }}",
			method: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				id: id,
				name: name,
				price: price,
				qty: qty
			},
			success: function(data) {
				// console.log(data);
				getCart();
			}, 
			error: function(data) {
				// console.log(data);
			}
		});
	}

	function getCart() {
		$.ajax({
			url: "{{ route('cart.getCart') }}",
			method: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				userId: "{{ auth()->user()->id }}"
			},
			success: function(data) {
				$('#cartBody').empty();
				var obj = Object.values(data);
				var cart = Object.values(obj[0]);
				// console.log(obj);
				$('#totalProduct').text(obj[1] + ' Produk');
				$('#totalQty').text(obj[2] + ' Item');
				$('#totalSub').text('Rp. ' + obj[3]);
				$('#totalAkhir').text('Rp. ' + obj[3]);

				mustPay = obj[3];

				var cash = $('#cash').val();
				var charge = parseFloat(cash) - (parseFloat(mustPay) - parseFloat(disc));
				$('#charge').text('Rp. ' + charge);

				if (cart.length == 0) {
					$('#cartBody').append(`
						<tr id="emptyCart">
							<td colspan="4">
								Belum Ada Produk dalam Keranjang
							</td>
						</tr>
					`);
				} else {
					cart.forEach(data => {
						$('#cartBody').append(`
							<tr>
								<td>
									<h5>${data.name}</h5>
								</td>
								<td id="qtyItem${data.id}">
									<h5>${data.quantity} &ensp; &ensp; <a href="javascript:void(0)" class="glyphicon glyphicon-pencil" onclick="editQty(${data.id}, '${data.name}', ${data.quantity}, ${data.price})"></a></h5>
								</td>
								<td>
									<h5 id="priceItem${data.id}">Rp. `+ parseFloat(data.price) * parseFloat(data.quantity) +`</h5>			
								</td>
								<td class="text-center">
									<a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="deleteFromCart(${data.id})">
										<i class="fa fa-trash"></i>
									</a>
								</td>
							</tr>
						`);
					});
				}
			}
		});
	}

	function countCharges() {
		cash = $('#cash').val();
		if (cash == undefined) {
			cash = 0
		}
		chrgs = parseFloat(cash) - (parseFloat(mustPay) - parseFloat(disc));
		$('#charge').text('Rp. '+chrgs);

		if (chrgs < 0) {
			$('#end').attr("disabled", true)
		} else {
			$('#end').attr("disabled", false);
		}
	}

	function countDiscount() {
		var discount = $('#discount').val();
		if (discount == '') {
			disc = 0;
		} else if (parseFloat(mustPay) < parseFloat(discount)) {
			disc = 0;
			alert('Diskon Melebihi Jumlah Kotor !');
			$('#discount').val('');
		} else {
			disc = $('#discount').val();
			var total = parseFloat(mustPay) - parseFloat(disc)
			$('#totalAkhir').text('Rp. ' + total);
		}
		countCharges();
	}

</script>

<script> 
	$(document).ready(function() {
		var cash = $('#cash').val();
		var mustPay = "{{ Cart::session(auth()->user()->id)->getTotal() }}";
		var charges = parseFloat(cash) - parseFloat(mustPay);
		$('#charge').text('Rp. '+charges);

		$('#search').on('keyup', function() {
			var searching = $('#search').val();
			if (searching !== '') {
				$('.category').hide();
				$('.searched').show();
			} else {
				$('#notFound').hide();
				$('#searchPanel').hide();
				$('.category').show();
				$('.searched').hide();
			}
		});

		$('#search').on('keydown', function(event) {
			if (event.keyCode == 13) {
				var searching = $('#search').val();
				if (searching !== '') {
					cari(searching);
				} else {
					alert('Masukan Keyword Pencarian !');
				}
			}
		});

		$('.portlet-body').on('click', '.product', function() {
			var id = $(this).data('id');
			var name = $(this).data('name');
			var harga = $(this).data('price');
			
			$('#cart > tbody:last-child').append(`
				<tr id="item` + no + `">
					<td>
						<h5>`+ name +`</h5>
					</td>
					<td>
						<input type="number" name="qty" onkeyup="qtyHarga(` + no + `, `+ harga +`)" id="qty` + no + `" class="form-control qtyProduct" style="border-radius: 0px;" min="1" value="1" data-id="${id}" data-name="${name}" data-price="${harga}">	
					</td>
					<td>
						<h5 id="price`+no+`">Rp. `+ harga +`</h5>			
					</td>
					<td class="text-center">
						<button type="button" onclick="deleteRow(` + no + `)" class="btn btn-danger btn-sm">
							<i class="fa fa-times"></i>
						</button>
					</td>
				</tr>
			`);

			$('#qty'+no).focus();
			no++;
		});


		$('#cart').on('keydown','.qtyProduct', function(event) {
			if (event.keyCode == 13) {
				var id = $(this).data('id');
				var nama = $(this).data('name');
				var price = $(this).data('price');
				var qty = $(this).val();

				// Unique Identifier for RowID Cart If Needed
				// var ra = Math.round(Math.random() * 1000);
				// var nd = Math.round(Math.random() * 100);

				addToCart(id, nama, price, qty);
			}
		});

		$('#cart').on('keydown','.updateQtyProduct', function(event) {
			if (event.keyCode == 13) {
				var id = $(this).data('id');
				var nama = $(this).data('name');
				var price = $(this).data('price');
				var qty = $(this).val();

				// Unique Identifier for RowID Cart If Needed
				// var ra = Math.round(Math.random() * 1000);
				// var nd = Math.round(Math.random() * 100);

				updateQtyCart(id, nama, price, qty);
			}
		});

	});
</script>

<script>
	$(document).ready(function() {
		countCharges();  
		$('#end').on('click', function() {
			var pay = $('#cash').val();
			$.ajax({
				url: "{{ route('pos.store') }}",
				method: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					cash: cash,
					discount: disc,
					charge: chrgs,
				},
				success: function(data) {
					if (data.code == '400') {
						alert(data.message);
					} else {
						alert(data.message);
						$('#cash').val('0');
						$('#discount').val('0');
						getCart();
					}
				}
			});
		});
	});
</script>

@endsection