@extends('backend.layout.master')

@section('judul')
	Management Produk
@endsection

@section('content')
<div class="col-12 col-md-12">
	<div class="card card-dark">
		<div class="card-header">
			<h4 class="card-title">
				Data Produk
			</h4>
			<div class="card-tools">
				<a href="{{ route('product.create') }}" class="btn btn-xs btn-primary">
					<i class="fa fa-plus"></i>
					Tambah Data Kategori
				</a>
			</div>
		</div>
		<div class="card-body">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-hover data-table">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama Produk</th>
								<th>Kategori Produk</th>
								<th width="20%">Deskripsi</th>
								<th>Stok</th>
								<th>Harga</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
	
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>
	$(document).ready(function() {
		var table = $('.data-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ route('product.index') }}",
			columns: [
				{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
				{ data: 'name', name: 'name' },
				{ data: 'category.name', name: 'category.name' },
				{ data: 'description', name: 'description' },
				{ data: 'stock', name: 'stock' },
				{ data: 'price', name: 'price' },
				{
					data: 'action',
					name: 'action',
					orderable: false,
					searchable: false
				}
			]
		})
	});
</script>
@endsection