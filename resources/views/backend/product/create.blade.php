@extends('backend.layout.master')

@section('judul')
		Form Tambah Produk
@endsection

@section('content')
<div class="col-12">
	<div class="card card-dark">
		<div class="card-header">
		</div>
		<div class="card-body">
			<form action="{{ route('product.create') }}" method="post">
				<div class="col-6">
					<div class="form-group">
						<label for="">Nama Produk : </label>
						<input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" value="{{ old('name') }}" required>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="">Kategori Produk : </label>
						<select name="category_id" id="category_id" class="form-control select2" data-placeholder="Pilih Kategori Produk" style="width: 100%;" required>
						</select>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>
	$('.select2')
    .select2()
    .on('select2:open', () => {
        $(".select2-results:not(:has(a))").append('<a href="#" style="padding: 6px;height: 20px;display: inline-table;">Create new item</a>');
		})

	$(document).ready(function() {
		setTimeout(() => {
			$.ajax({
				url: "{{ route('product.create') }}",
				method: 'GET',
				data: {
					// test: 'ini bisa di-pake !'
				},
				success: function(data) {
					console.log(data);
				}
			});
		}, 1000);
	});
</script>
@endsection