<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Backend</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('assets/global/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <!-- Fonts -->
    <link href="{{ asset('css/opensans.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet"
        type="text/css" />
    <!-- Styles -->
    <link href="{{ asset('assets/global/plugins/pace/themes/pace-theme-flash.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/css/components-rounded.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/layouts/layout/css/themes/darkblue.min.css') }}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{ asset('favicon.png') }}" />
		
		<!-- DataTables -->
		<link rel="stylesheet" href="{{ asset('') }}plugins/datatables-bs4/css/dataTables.bootstrap4.css">

		@yield('css')
</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white {{ Request::is('pos') ? 'page-sidebar-closed':'' }}">
    <div id="app">
        <div class="page-header navbar navbar-fixed-top">
            <div class="page-header-inner ">
                <div class="page-logo">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('logo-backend.png') }}" alt="Logo Aplikasi" class="logo-default"
                            style="height: 27px; margin-top: 10px" />
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                        <span></span>
                    </div>
                </div>
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
                    data-target=".navbar-collapse">
                    <span></span>
                </a>
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                data-close-others="true">
                                <img alt="" class="img-circle"
                                    src="{{ asset('assets/layouts/layout/img/avatar.png') }}" />
                                <span class="username username-hide-on-mobile"> {{ Auth::user()->name }} </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="{{ url('backend/users/changepassword') }}">
                                        <i class="fa fa-key"></i> Ganti Kata Sandi
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out"></i> Keluar
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container">
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <ul class="page-sidebar-menu page-header-fixed {{ Request::is('pos*') ? 'page-sidebar-menu-closed':'' }}" data-keep-expanded="false" data-auto-scroll="true"
                        data-slide-speed="200" style="padding-top: 2px;">
                        <li class="sidebar-toggler-wrapper hide">
                            <div class="sidebar-toggler">
                                <span></span>
                            </div>
                        </li>
                        <li class="heading">
                            <h3 class="uppercase">Main Menu</h3>
                        </li>
                        <li class="nav-item start {{ Request::is('Admin') ? 'active' : '' }}">
                            <a href="{{ url('/Admin') }}" class="nav-link nav-toggle">
                                <i class="fa fa-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="nav-item start {{ Request::is(['backend/currency*']) ? 'active' : '' }}">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-edit"></i>
                                <span class="title">Master Data</span>
                                <span class="selected"></span>
                                <span class="arrow {{ Request::is(['backend/currency*']) ? 'open' : '' }}"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item {{ Request::is('backend/currency*') ? 'active' : '' }}">
                                    <a href="{{ route('category.index') }}" class="nav-link ">
                                        <span class="title">Kategori</span>
                                    </a>
																</li>
																<li class="nav-item {{ Request::is('backend/currency*') ? 'active' : '' }}">
																	<a href="{{ route('product.index') }}" class="nav-link ">
																			<span class="title">Produk</span>
																	</a>
																</li>
                            </ul>
                        </li>
                        <li class="nav-item start {{ Request::is('pos') ? 'active' : '' }}">
                            <a href="{{ url('/pos') }}" class="nav-link nav-toggle">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Point of Sales</span>
                                <span class="selected"></span>
                            </a>
												</li>
                        <li class="nav-item start {{ Request::is('pos-list') ? 'active' : '' }}">
                            <a href="{{ url('/pos-list') }}" class="nav-link nav-toggle">
                                <i class="fa fa-list"></i>
                                <span class="title">Data Point of Sales</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li
                            class="nav-item start {{ Request::is(['backend/users*', 'backend/permissions*']) ? 'active' : '' }}">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-settings"></i>
                                <span class="title">Pengaturan</span>
                                <span class="selected"></span>
                                <span
                                    class="arrow {{ Request::is(['backend/users*', 'backend/permissions*']) ? 'open' : '' }}"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item {{ Request::is('backend/users*') ? 'active' : '' }}">
                                    <a href="{{ url('backend/users') }}" class="nav-link ">
                                        <span class="title">Pengguna</span>
                                    </a>
                                </li>
                                <li class="nav-item {{ Request::is('backend/permissions*') ? 'active' : '' }}">
                                    <a href="{{ url('backend/permissions') }}" class="nav-link ">
                                        <span class="title">Hak Akses</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="page-content-wrapper">
                <div class="page-content">
										<div class="container-fluid">
											<div class="row">
												@yield('content')
											</div>
										</div>
                </div>
            </div>
        </div>
        <div class="page-footer pull-right">
            <div class="page-footer-inner">
							Copyright 2020 
						</div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
		</div>
		<!-- Scripts -->
    <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/layout/scripts/demo.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>

    <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
    <script src="{{ asset('assets/moment/moment.min.js') }}"></script>

		
		<!-- DataTables -->
		<script src="{{ asset('') }}plugins/datatables/jquery.dataTables.js"></script>
		<script src="{{ asset('') }}plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

		<script>
			$('.select2').select2();
		</script>

    @yield('javascript')
</body>

</html>
